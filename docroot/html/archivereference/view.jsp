<%--
/**
* Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the Free
* Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
* details.
*/
--%>

<!-- 
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.ui.core.js"></script>
<script type="text/javascript" src="js/jquery.ui.datepicker-ru.min.js"></script>
<script type="text/javascript" src="js/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
<script type="text/javascript" src="js/javascript.js"></script>
 -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<div id="ar">

<div>Проверка версии</div>
<a id="www" href="http://jquery.com/">jQuery</a>

<form accept-charset="utf-8" id="ar_form"  enctype="multipart/form-data" method="post" action="<portlet:resourceURL></portlet:resourceURL>">
	<h3>Укажите цель и характер запроса</h3>
	<label>Цель получения архивного документа<div class="required">*</div></label>
	<div id="layout_purpose">
		<input id="pension" type="radio" name="purpose" value="оформление, перерасчет пенси">оформление, перерасчет пенсии;<br>
		<input id="aids" type="radio" name="purpose" value="оформление пособий">оформление пособий;<br>
		<input id="other" type="radio" name="purpose" value="other">иное:<br>
		<input id="other_text" class="valid" type="text" name="other_text">
	</div>
	
	<label>Что необходимо подтвердить?<div class="required">*</div></label>
	<div id="layout_what_needed_confirm">
		<input id="facts_applicant" type="radio" name="what_needed_confirm" value="факты о работе заявителя">факты о работе заявителя:<br>
			<div id="layout_facts_applicant">
				<input id="work_experience" type="checkbox" name="what_needed_confirm1" value="стаж работы">стаж работы;<br>
				<input id="preferential_work_experience" type="checkbox" name="what_needed_confirm2" value="льготный трудовой стаж работы">льготный трудовой стаж работы;<br>
				<input id="wages" type="checkbox" name="what_needed_confirm3" value="размер заработной платы">размер заработной платы;<br>
				<input id="fact_change_org" type="checkbox" name="what_needed_confirm4" value="факт переименования, реорганизации, ликвидации организации">факт переименования, реорганизации, ликвидации организации;<br>
			</div>
		<input id="fact_school" type="radio" name="what_needed_confirm" value="факт обучения в школе">факт обучения в школе.<br>
	</div>
	
	<label>Где работал заявитель?<div class="required">*</div></label>
	<div id="layout_where_applicant_worked">
		<input id="education_system" type="radio" name="where_applicant_worked" value="мунципальная система образования">мунципальная система образования;<br>
		<input id="otherwise_org" type="radio" name="where_applicant_worked" value="иные ликвидированные, реорганизованные организации города Омска">иные ликвидированные, реорганизованные организации города Омска.<br>
	</div>
	
	<label>Вид архивного документа<div class="required">*</div></label>
	<div id="layout_view_document_archive">
		<input id="archive_reference" type="radio" name="view_document_archive" value="архивная справка">архивная справка;<br>
		<input id="copy_archive_document" type="radio" name="view_document_archive" value="копия архивного документа">копия архивного документа;<br>
		<input id="archive_extract" type="radio" name="view_document_archive" value="архивная выписка">архивная выписка.<br>
	</div>
	
	<p>Архивная справка содержит сведения из документов, находящихся на хранении. Архивная выписка предполагает объемную цитату из архивного документа. Копия архивного документа в точности воспроизводит документ, находящийся на хранении.</p>
	<p>При оформлении пенсии и пособий в большинстве случаев достаточно архивной справки.</p>
	
	<h3>Введите сведения о заявителе</h3>
	<div id="layout_info_applicant">
		<input id="individual_applicant" type="radio" name="info_applicant" value="individual">физическое лицо;<br>
		<input id="legal_applicant" type="radio" name="info_applicant" value="legal">юридическое лицо.<br>
	</div>
	
	<div id="layout_individual">
		<label>Заявитель — физическое лицо<div class="required">*</div></label>
		<input id="individual_last_name" class="valid" type="text" name="individual_last_name">
		<label>Фамилия</label>
		<input id="individual_first_name" class="valid" type="text" name="individual_first_name">
		<label>Имя</label>
		<input id="individual_middle_name" type="text" name="individual_middle_name">
		<label>Отчество</label>
		
		<input id="individual_change_name" type="checkbox" name="individual_change_name" value="Заявитель менял фамилию, имя или отчество">Заявитель менял фамилию, имя или отчество
		<div id="layout_icn">
			<input id="text_icn" type="text" name="text_icn">
			<label>Перечислите иные фамилии, имена и отчества через запятую</label>
		</div>
		
		<label>Паспорт<div class="required">*</div></label>
		<input id="individual_series_passport" class="valid" type="text" name="individual_series_passport">
		<label>Серия</label>
		<input id="individual_number_passport" class="valid" type="text" name="individual_number_passport">
		<label>Номер</label>
		
		<label>Кем выдан паспорт<div class="required">*</div></label>
		<input id="individual_who_give_passport" class="valid" type="text" name="individual_who_give_passport">
		
		<label>Дата выдачи паспотра<div class="required">*</div></label>
		<input id="individual_date_issue_passport" class="valid date" type="text" name="individual_date_issue_passport">
		
		<label>Адрес<div class="required">*</div></label>
		<input id="individual_сountry" class="valid" type="text" name="individual_сountry" value="Россия">
		<label>Страна</label>
		<input id="individual_region" type="text" name="individual_region" value="Омская область">
		<label>Регион</label>
		<input id="individual_city" class="valid" type="text" name="individual_city" value="Омск">
		<label>Город</label>
		<input id="individual_street" class="valid" type="text" name="individual_street">
		<label>Улица</label>
		<input id="individual_home" class="valid" type="text" name="individual_home">
		<label>Дом</label>
		<input id="individual_housing" type="text" name="individual_housing">
		<label>Корпус</label>
		<input id="individual_apartment" type="text" name="individual_apartment">
		<label>Квартира</label>
		
		<div id="layout_li_dates_birth">
			<label>Даты рождения детей<div class="required">*</div></label>
			<input id="individual_dates_birth1" class="valid date" type="text" name="individual_dates_birth1">
			<input id="b_add_idb" type="button" value="Добавить">
			<input id="b_remove_idb" type="button"value="Удалить">
		</div>
		
		<label>Телефон<div class="required">*</div></label>
		<input id="individual_number_telephone" class="valid" type="text" name="individual_number_telephone">
		<label>Для возможности оперативной связи необходимо указывать контактные номера телефонов (желательно и сотовый, и стационарный).</label>
		
		<label>Адрес электронной почты<div class="required">*</div></label>
		<input id="individual_email" class="valid email" type="text" name="individual_email">
		<label>На указанный адрес будет направлено уведомление о готовности документа с указанием адреса службы одного окна, по которому будет выдаваться запрашиваемый документ, и телефона для справок.</label>
	</div>
	
	<div id="layout_legal">
		<label>Заявитель — юридическое лицо<div class="required">*</div></label>
		<input id="legal_full_name" class="valid" type="text" name="legal_full_name">
		<label>Полное наименование</label>
		
		<label>Реквизиты<div class="required">*</div></label>
		<input id="legal_BIN" class="valid" type="text" name="legal_BIN">
		<label>ОГРН</label>
		<input id="legal_INN" class="valid" type="text" name="legal_INN">
		<label>ИНН</label>
		
		<label>Руководитель<div class="required">*</div></label>
		<input id="legal_last_name" class="valid" type="text" name="legal_last_name">
		<label>Фамилия</label>
		<input id="legal_first_name" class="valid" type="text" name="legal_first_name">
		<label>Имя</label>
		<input id="legal_middle_name" type="text" name="legal_middle_name">
		<label>Отчество</label>
		
		<label>Должность руководителя<div class="required">*</div></label>
		<input id="legal_post_head" class="valid" type="text" name="legal_post_head">
		
		<label>Почтовый адрес<div class="required">*</div></label>
		<input id="legal_сountry" class="valid" type="text" name="legal_сountry" value="Россия">
		<label>Страна</label>
		<input id="legal_region" type="text" name="legal_region" value="Омская область">
		<label>Регион</label>
		<input id="legal_city" class="valid" type="text" name="legal_city" value="Омск">
		<label>Город</label>
		<input id="legal_street" class="valid" type="text" name="legal_street">
		<label>Улица</label>
		<input id="legal_home" class="valid" type="text" name="legal_home">
		<label>Дом</label>
		<input id="legal_housing" type="text" name="legal_housing">
		<label>Корпус</label>
		<input id="legal_apartment" type="text" name="legal_apartment">
		<label>Квартира</label>
		
		<label>Телефон<div class="required">*</div></label>
		<input id="legal_number_telephone" class="valid" type="text" name="legal_number_telephone">
		
		<label>Адрес электронной почты<div class="required">*</div></label>
		<input id="legal_email" class="valid email" type="text" name="legal_email">
		<label>На указанный адрес будет направлено уведомление о готовности документа с указанием адреса службы одного окна, по которому будет выдаваться запрашиваемый документ, и телефона для справок.</label>
	</div>
	
	<div id="person_info">
		<h3>Введите данные лица, в отношении которого запрашивается информация</h3>
		<input id="pi_last_name" class="valid" type="text" name="pi_last_name">
		<label>Фамилия</label>
		<input id="pi_first_name" class="valid" type="text" name="pi_first_name">
		<label>Имя</label>
		<input id="pi_middle_name" type="text" name="pi_middle_name">
		<label>Отчество</label>
		<input id="pi_change_name" type="checkbox" name="pi_change_name">Заявитель менял фамилию, имя или отчество
		<input id="text_picn" type="text" name="text_picn">
		<label>Перечислите иные фамилии, имена и отчества через запятую</label>
		
		<div id="layout_pi_dates_birth">
			<label>Даты рождения детей<div class="required">*</div></label>
			<input id="pi_dates_birth1" class="valid date" type="text" name="pi_dates_birth1">
			<input id="b_add_pdb" type="button" value="Добавить">
			<input id="b_remove_pdb" type="button" value="Удалить">
		</div>
	</div>
	
	<div id="layout_info_req_org">
		<h3>Введите сведения по запрашиваемой организации</h3>
		<label>Сведения о запрашиваемой организации: полное и сокращенное название, переименования, ведомственная принадлежность<div class="required">*</div></label>
		<input id="info_req_org" class="valid" type="text" name="info_req_org"><br>
		
		<label>Дата начала трудовой деятельности в организации<div class="required">*</div></label>
		<input id="start_work_org" class="valid date" type="text" name="start_work_org">
		
		<label>Дата окончания трудовой деятельности в организации<div class="required">*</div></label>
		<input id="end_work_org" class="valid date" type="text" name="end_work_org">
		
		<div id="layout_position">
			<label>Занимаемая должность<div class="required">*</div></label>
			<input id="position" class="valid" type="text" name="position"><br>
		</div>
		
		<label>Копия трудовой книжки (при наличии)</label>
		<input id="copy_work_book" type="file" name="copy_work_book"><br>
		<label>Отсканируйте титульную страницу и страницы, имеющие отношение к запросу. Важно понимать, что любые пометки, печати, штампы могут оказаться полезными в поиске запрашиваемых сведений.</label>
		
		<label>Иные сведения, облегчающие поиск необходимой информации<div class="required">*</div></label>
		<input id="search_info_req_org" class="valid" type="text" name="search_info_req_org"><br>
	</div>
	
	<div id="layout_info_req_school">
		<h3>Введите сведения по запрашиваемой школе</h3>
		<label>Наименование школы<div class="required">*</div></label>
		<input id="name_school" class="valid" type="text" name="name_school"><br>
		
		<label>Номер школы<div class="required">*</div></label>
		<input id="num_school" class="valid" type="text" name="num_school"><br>
		
		<label>Дата начала обучения в школе<div class="required">*</div></label>
		<input id="start_study_school" class="valid date" type="text" name="start_study_school">
		
		<label>Дата окончания обучения в школе<div class="required">*</div></label>
		<input id="end_study_school" class="valid date" type="text" name="end_study_school">
		
		<label>Иные сведения, облегчающие поиск необходимой информации<div class="required">*</div></label>
		<input id="search_info_req_school" class="valid" type="text" name="search_info_req_school"><br>
	</div>
	
	<%--
	<button type="submit" name="submitButton" value="submit">Отправить</button>
	<input id="submit-button" type="button" value="Отправить">
	--%>
	
	<!-- <button id="sb_send">Отправить1</button>  -->
	<input id="sb_send" type="button" value="Отправить">	
	
</form>

<div id="output1">AJAX response will replace this content.</div>

</div>
