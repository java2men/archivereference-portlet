
var $jq = jQuery.noConflict(true);

$jq(document).ready(function(){
	//$jq("#ar").
	//$jq("#ar_form").reset();
	//Сбросить элементы формы
	$jq("#ar_form")[0].reset();
	 
	/*$jq("#ar").block({ 
		message: "<h1>Processing</h1>", 
		css: { border: '3px solid #a00' } 	
	});*/
	
	
	function validate(element, next, classname) {
		//if ($jq(element).parent().attr("class") != classname) {
		//alert("hasClass="+$jq(element).parent().hasClass(classname));
		if (!$jq(element).parent().hasClass(classname)) {
			//alert("блядь сколько можно");
			$jq(element).wrap("<div class=\""+classname+"\"></div>");
			$jq(element).after(next);
		}
	}
	
	function unvalidate(element, classname) {
		//if ($jq(element).parent().attr("class") == classname) {
		if ($jq(element).parent().hasClass(classname)) {
			$jq(element).unwrap();
			if ((next = $jq(element).next()).attr("id") == "validator") next.remove();
		}
	}
	
	function checkemail(emailAddress) {
	    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	    return pattern.test(emailAddress);
	}
	
	function checkdate(date) {
	    var pattern = new RegExp(/^\d{1,2}\.\d{1,2}\.\d{4}$/);
	    return pattern.test(date);
	}
	
	$jq("#www").click(function(event){
		alert("As you can see, the link no longer took you to jquery.com");
		event.preventDefault();
		var msg;
		if (window.$jq) {
		    msg = 'You are running jQuery version: ' + $jq.fn.jquery;
		} else {
		    msg = 'jQuery is not installed';
		}
		alert(msg);
		alert('You are running jQuery version: ' + jQuery.fn.jquery);
	});
	
	$jq("#other_text").hide();
	$jq("#layout_facts_applicant").hide();
	
	//навешать datepicker
	$jq("#ar_form .valid.date").datepicker(
			{
				yearRange: '1900:2100',
				changeMonth: true,
				changeYear: true,
				showAnim: '',
				constrainInput: true,
				//showOn: "button",
				//buttonImage: "ArchiveReference-portlet/css/images/calendar.gif",
				//buttonImageOnly: true
			}
	);
	
	$jq("#layout_individual").hide();
	$jq("#layout_legal").hide();
	$jq("#layout_icn").hide();
	$jq("#b_remove_idb").hide();
	
	$jq("#layout_info_req_org").hide();
	$jq("#layout_info_req_school").hide();
	
	$jq("#layout_position").hide();
	$jq("#layout_li_dates_birth").hide();
	$jq("#layout_pi_dates_birth").hide();
	
	$jq("#person_info").hide();
	
	var necessarily = "<div id=\"validator\">Поле обязательно для заполнения.</div>";
	var notselected = "<div id=\"validator\">Не выбрано значение.</div>";
	var bademail = "<div id=\"validator\">Адрес электронной почты введен некорректно.</div>";
	var baddate = "<div id=\"validator\">Дата введена некорректно.</div>";

	//Навешать событие потеря фокуса на элементы класса valid для формы ar_form с целью обозначения валидации
	$jq("#ar_form .valid").live("blur", function(){
		
		//если поле для даты или email или валидное, и непустое, то не обрабатывать событие потери фокуса
		if ( ($jq(this).hasClass("date") || $jq(this).hasClass("email")) &&  $jq(this).val() != "" ) {
			return;
		}
		
		if ($jq(this).val() == "" ) {
			//удалить предыдущую валидацию
			unvalidate($jq(this), "light-error");
			//создать новую валидацию
			validate($jq(this), necessarily, "light-error");
		} else {
			//Удалить валидации
			unvalidate($jq(this), "light-error");
		}
		
		
	});
	
	//навешать события change на поля с датой
	$jq("#ar_form .date").live("change", function(){
		
		//если поле пустое, то приостановить обаботчик 
		if ($jq(this).val() == "") return;
		
		//проверка корректности даты
		if (checkdate($jq(this).val())) {
			//удалить валидацию
			unvalidate($jq(this), "light-error");
		} else {
			//удалить предыдущую валидацию
			unvalidate($jq(this), "light-error");
			//создать валидацию
			validate($jq(this), baddate, "light-error");
		}
		
	});
	
	//навешать события change на поля с email
	$jq("#ar_form .email").live("change", function(){
		
		//если поле пустое, то приостановить обаботчик
		if ($jq(this).val() == "") return;
		
		//проверка корректности email
		if (checkemail($jq(this).val())) {
			//удалить валидацию
			unvalidate($jq(this), "light-error");
		} else {
			//удалить предыдущую валидацию
			unvalidate($jq(this), "light-error");
			//создать валидацию
			validate($jq(this), bademail, "light-error");
		}
		
	});
	
	//Изменение состояния "Цель получения архивного документа"
	$jq("#layout_purpose :radio").bind("change", function(event){
		//Если валидатор создан, то удалить его
		unvalidate($jq("#layout_purpose"), "light-error");
		
		var select = $jq("input[name='purpose']:checked").val();
		if (select == "other") {
			//показать, с учетом валидатора
			if ((parent = $jq("#other_text").parent()).attr("class") == "light-error") parent.show();
			else $jq("#other_text").show();
		} else {
			//скрыть, с учетом валидатора
			if ((parent = $jq("#other_text").parent()).attr("class") == "light-error") parent.hide();
			else $jq("#other_text").hide();
		}
	});
	
	//Изменение состояния "Что необходимо подвердить"
	$jq("#layout_what_needed_confirm :radio").bind("change", function(event){
		//Если валидатор создан, то удалить его
		unvalidate($jq("#layout_what_needed_confirm"), "light-error");
		
		var select = $jq("input[name='what_needed_confirm']:radio:checked").val();
		if (select == "факты о работе заявителя") {
			$jq("#layout_facts_applicant").show();
			$jq("#layout_info_req_org").show();
			$jq("#layout_info_req_school").hide();
		}
		
		if (select == "факт обучения в школе") {
			$jq("#layout_facts_applicant").hide();
			$jq("#layout_info_req_school").show();
			$jq("#layout_info_req_org").hide();
		}
		
	});
	
	//Выбрать/Не выбрать состояния "Факты о работе заявителя:"
	$jq("#layout_facts_applicant :checkbox").bind("change", function(){
		//Если валидатор создан, то удалить его
		unvalidate($jq("#layout_facts_applicant"), "light-error");
		
		//Если не выбрано ни одно значение
		if ($jq("#layout_facts_applicant :checkbox:checked").length == 0) {
			validate($jq("#layout_facts_applicant"), notselected, "light-error");
		}
		
		
		/*Если выбраны/не выбраны 
		"стаж работы", "льготный трудовой стаж работы", "размер заработной платы", 
		 * то включить/выключить "Занимаемая должность" в "Сведениях по запрашиваемой организации"*/
		if ($jq("#work_experience").is(":checked") 
				| $jq("#preferential_work_experience").is(":checked") 
				| $jq("#wages").is(":checked")) {
			$jq("#layout_position").show();
		} else {
			$jq("#layout_position").hide();
		}
		/*Если выбрано/не выбрано "льготный трудовой стаж работы", 
		то включить/выключить "Даты рождения детей" в "Физическом лице" и 
		"Данные лица, в отношении которого запрашивается информация"
		*/
		if($jq("#preferential_work_experience").is(":checked")) {
			$jq("#layout_li_dates_birth").show();
			$jq("#layout_pi_dates_birth").show();
		} else {
			$jq("#layout_li_dates_birth").hide();
			$jq("#layout_pi_dates_birth").hide();
		}
	});
	
	//Изменение состояния "Где работал заявитель"
	$jq("#layout_where_applicant_worked :radio").bind("change", function(event){
		//Если валидатор создан, то удалить его
		unvalidate($jq("#layout_where_applicant_worked"), "light-error");
	});
	
	//Изменение состояния "Вид архивного документа"
	$jq("#layout_view_document_archive :radio").bind("change", function(event){
		//Если валидатор создан, то удалить его
		unvalidate($jq("#layout_view_document_archive"), "light-error");
	});
	
	/*Скрыть/раскрыть физическое или юридическое лицо*/
	$jq("#layout_info_applicant :radio").change(function(event){
		//Если валидатор создан, то удалить его
		unvalidate($jq("#layout_info_applicant"), "light-error");
		
		var select = $jq("input[name='info_applicant']:radio:checked").val();
		if (select == "individual") {
			$jq("#layout_individual").show();
			$jq("#layout_legal").hide();
			$jq("#person_info").hide();
		}
		if (select == "legal") {
			$jq("#layout_legal").show();
			$jq("#layout_individual").hide();
			$jq("#person_info").show();
		}
	});
	
	/*Скрыть/раскрыть "Заявитель менял фамилию, имя или отчество"*/
	$jq("#individual_change_name").change(function(event){
		var select = $jq("#individual_change_name:checked").val();
		if (select) {
			$jq("#layout_icn").show();
		} else {
			$jq("#layout_icn").hide();
		}		
	});
	
	//Добавить даты рождения детей в "физическом лице"
	$jq("#b_add_idb").click(function(event){
		var n = $jq("input[id^='individual_dates_birth']").size();
		n++;
		if ($jq('#b_remove_idb').is(':hidden')) $jq("#b_remove_idb").show();
		$jq("#b_add_idb").before("<input id=\"individual_dates_birth"+n+"\" class=\"valid date\" type=\"text\" name=\"individual_dates_birth"+n+">");
		//Навешать календарь на остальные текстовые поля 
		$jq("#individual_dates_birth"+n).datepicker(
			{
				yearRange: '1900:2100',
				changeMonth: true,
				changeYear: true,
				showAnim: '',
				constrainInput: true,
				//showOn: "button",
				//buttonImage: "ArchiveReference-portlet/css/images/calendar.gif",
				//buttonImageOnly: true
			}
		);
	});
	
	//Удалить даты рождения детей в "физическом лице"
	$jq("#b_remove_idb").click(function(event){
		var n = $jq("input[id^='individual_dates_birth']").size();
		$jq("#individual_dates_birth"+n).remove();
		if ((n-1) == 1) {
			$jq("#b_remove_idb").hide();
			return;
		}
	});
	
	//Навешать календарь на первое текстовое поле в "Введите данные лица, в отношении которого запрашивается информация"
	$jq("#pi_dates_birth"+1).datepicker(
		{
			yearRange: '1900:2100',
			changeMonth: true,
			changeYear: true,
			showAnim: '',
			constrainInput: true,
			//showOn: "button",
			//buttonImage: "ArchiveReference-portlet/css/images/calendar.gif",
			//buttonImageOnly: true
		}
	);
	
	//Добавить даты рождения детей в "Введите данные лица, в отношении которого запрашивается информация"
	$jq("#b_add_pdb").click(function(event){
		var n = $jq("input[id^='pi_dates_birth']").size();
		n++;
		if ($jq('#b_remove_pdb').is(':hidden')) $jq("#b_remove_pdb").show();
		$jq("#b_add_pdb").before("<input id=\"pi_dates_birth"+n+"\" class=\"valid date\" type=\"text\" name=\"pi_dates_birth"+n+">");
		
		//Навешать календарь на остальные текстовые поля 
		$jq("#pi_dates_birth"+n).datepicker(
			{
				yearRange: '1900:2100',
				changeMonth: true,
				changeYear: true,
				showAnim: '',
				constrainInput: true,
				//showOn: "button",
				//buttonImage: "ArchiveReference-portlet/css/images/calendar.gif",
				//buttonImageOnly: true
			}
		);
	});
	
	//Удалить даты рождения детей в "Введите данные лица, в отношении которого запрашивается информация"
	$jq("#b_remove_pdb").click(function(event){
		var n = $jq("input[id^='pi_dates_birth']").size();
		$jq("#pi_dates_birth"+n).remove();
		if ((n-1) == 1) {
			$jq("#b_remove_pdb").hide();
			return;
		}
	});
	
	//работа с формой
	var options = {
		target:        '#output1',   // target element(s) to be updated with server response 
		//beforeSubmit:  showRequest,  // pre-submit callback 
		success:       showResponse,  // post-submit callback 
		
		// other available options: 
		//url:       '<portlet:actionURL></portlet:actionURL>',         // override for form's 'action' attribute 
		//type:      html        // 'get' or 'post', override for form's 'method' attribute 
		//dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
		//clearForm: true        // clear all form fields after successful submit 
		//resetForm: true        // reset the form after successful submit 
		 
		// $.ajax options can be used here too, for example: 
		//timeout:   3000 
	};
	
	// bind form using 'ajaxForm' 
	//$jq('#ar_form').ajaxForm(options);
	$jq('#ar_form').ajaxForm({
        // target identifies the element(s) to update with the server response 
        //target: '#output1',
        //clearForm: true,
        //resetForm: true,
        forceSync: true,
        async: true,
        type:  "POST", 
        //dataType:  'script',        // 'xml', 'script', or 'json' (expected server response type)
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        /*success: function() {
            //$jq('#output1').fadeIn('slow');
            $jq('#ar_form').hide();
            alert(responseText);
        },*/
        
        beforeSubmit: function() {
        	$jq.blockUI({ message: '<h1>Отправка данных...</h1>' });
		}, 
        
		success: processResponse,
		
		error: processResponse
    });
	
	var options = {
        // target identifies the element(s) to update with the server response 
        //target: '#output1',
        //clearForm: true,
        //resetForm: true,
        //forceSync: true,
        //async: true,
        //type:  "POST", 
        //dataType:  'script',        // 'xml', 'script', or 'json' (expected server response type)
        // success identifies the function to invoke when the server response 
        // has been received; here we apply a fade-in effect to the new content 
        /*success: function() {
            //$jq('#output1').fadeIn('slow');
            $jq('#ar_form').hide();
            alert(responseText);
        },*/
        
        beforeSubmit: function() {
        	$jq.blockUI({ message: '<h1>Отправка данных...</h1>' });
		}, 
        
		success: processResponse,
		
		error: processResponse
    };
	
	//функция удаления символа конца строки
	function parseResponse(response) {
		if (response.charAt(response.length-1) == '\n') return response.substring(0, response.length-1);
		return response;
	};
	
	//Разблокировать интерфейс как остановится ajax запрос
	$jq(document).ajaxStop($jq.unblockUI);
	
	error_not_file_format = 
		"<div id=\"error_msg\">" +
		"<h2>Произошла ошибка!</h2>" +
		"<div>Формат файла не поддерживается.</div>"+
		"<div>Данные формы не были отправлены. Пожалуйста, обратите внимание, что возможна загрузка только файлов следующих форматов: zip, pdf, doc (docx), jpg (jpeg), png, gif, bmp, tif (tiff), при этом размер каждого загружаемого файла не должен превышать 5 Мб.</div>"+
		"<div>Если ошибка повторяется, то обратитесь за помощью по телефону (381-2) 24-40-78 (с понедельника по четверг&nbsp;— с 9:00 до 18:00, в пятницу&nbsp;— с 9:00 до 16:30) или по электронной почте <a href=\"mailto:feedback@admomsk.ru\">feedback@admomsk.ru</a>.</div>"+
		"</div>";

	error_file_more_max_size = 
		"<div id=\"error_msg\">" +
		"<h2>Произошла ошибка!</h2>" +
		"<div>Файл превышает максимальный допустимый размер.</div>" +
		"<div>Данные формы не были отправлены. Пожалуйста, обратите внимание, что возможна загрузка только файлов следующих форматов: zip, pdf, doc (docx), jpg (jpeg), png, gif, bmp, tif (tiff), при этом размер каждого загружаемого файла не должен превышать 5 Мб.</div>"+
		"<div>Если ошибка повторяется, то обратитесь за помощью по телефону (381-2) 24-40-78 (с понедельника по четверг&nbsp;— с 9:00 до 18:00, в пятницу&nbsp;— с 9:00 до 16:30) или по электронной почте <a href=\"mailto:feedback@admomsk.ru\">feedback@admomsk.ru</a>.</div>"+
		"</div>";
	
	sent_msg = 
		"<div id=\"sent_msg\">" +
		"<h2>Отправка завершена</h2>" +
		"<div>Данные формы успешно отправлены. Благодарим вас за использование этого сервиса портала. О порядке дальнейшего прохождения процедуры можно узнать в <a href=\"/web/guest/services/center\">службе одного окна</a>.</div>"+
		"</div>";
	
	//Функция обработки ответа с сервера
	function processResponse(responseText, statusText) {
		
		if (statusText == "success") {
			
			//$jq("#ar").children(":not(#ar_form)").remove();
			
			//строка без последнего символа перевода строки
			prrt = parseResponse(responseText);
			
			//если файл больше макс. размера
			if (prrt == "file_more_max_size") {
				//удалить все элементы кроме формы
				$jq("#ar").children(":not(#ar_form)").remove();
				//вставить сообщение об ошибке
				$jq("#ar_form").after(error_file_more_max_size);
			}
			//если файл имеет не поддерживаемый формат
			if (prrt == "not_file_format") {
				//удалить все элементы кроме формы
				$jq("#ar").children(":not(#ar_form)").remove();
				//вставить сообщение об ошибке
				$jq("#ar_form").after(error_not_file_format);
			}
			
			//если файл загружен успешно
			if (prrt == "file_success_uploaded") {
			}
			
			//если все параметры успешно получены и обработаны
			if (prrt.indexOf("success_get_all_parametrs") > -1) {
				//удалить элементы
				$jq("#ar").children().remove();
				//вставить сообщение об успешной отправке
				$jq("#ar").html(sent_msg);
			}
			
		}
		
	};
	
	//Обработчик кнопки отправить
	$jq("#sb_send").click(function(){
		
		//Проверить выбранные элементы в "Цель получения архивного документа"
		if ($jq("#layout_purpose :radio:checked").length == 0) {
			//Если валидатор не создан, то создать его
			validate($jq("#layout_purpose"), notselected, "light-error");
		}
		
		//Проверить выбранные элементы в "Что необходимо подтвердить?"
		if ($jq("#layout_what_needed_confirm :radio:checked").length == 0) {
			//Если валидатор не создан, то создать его
			validate($jq("#layout_what_needed_confirm"), notselected, "light-error");
		} else {
			//Если выбраны факты о работе заявителя
			if ($jq("#facts_applicant").is(":checked")) {
				//Проверить выбранные элементы в "факты о работе заявителя:"
				if ($jq("#layout_facts_applicant :checkbox:checked").length == 0) {
					//Если валидатор не создан, то создать его
					validate($jq("#layout_facts_applicant"), notselected, "light-error");
				}
			}
		}
		
		//Проверить выбранные элементы в "Где работал заявитель?"
		if ($jq("#layout_where_applicant_worked :radio:checked").length == 0) {
			//Если валидатор не создан, то создать его
			validate($jq("#layout_where_applicant_worked"), notselected, "light-error");
		}
		
		//Проверить выбранные элементы в "Вид архивного документа"
		if ($jq("#layout_view_document_archive :radio:checked").length == 0) {
			//Если валидатор не создан, то создать его
			validate($jq("#layout_view_document_archive"), notselected, "light-error");
		}
		
		//Проверить выбранные элементы в "Введите сведения о заявителе"
		if ($jq("#layout_info_applicant :radio:checked").length == 0) {
			//Если валидатор не создан, то создать его
			validate($jq("#layout_info_applicant"), notselected, "light-error");
		}
		
		/*сделать сброс фокуса для валидных и видимых элементов в форме, 
		чтобы найти не заполненные или не выбранные поля*/
		$jq("#ar_form .valid:visible").blur();
		
		//Если все элементы заполненны и выбраны, то отправить форму на сервер
		if ($jq("#ar_form .light-error:visible").length == 0) {
			$jq("#ar_form").submit();
			// return false to prevent normal browser submit and page navigation 
		    //return false;
		    
		    /*$jq('#ar_form').submit(function() { 
		        // submit the form 
		        $jq(this).ajaxSubmit(options); 
		        // return false to prevent normal browser submit and page navigation 
		        return false; 
		    });*/
		}
	});
	
	//$jq(document).ajaxStop($jq.unblockUI);
	
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form) {
	    // for normal html responses, the first argument to the success callback 
	    // is the XMLHttpRequest object's responseText property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'xml' then the first argument to the success callback 
	    // is the XMLHttpRequest object's responseXML property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'json' then the first argument to the success callback 
	    // is the json data object returned by the server 
	 
	    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
	        '\n\nThe output div should have already been updated with the responseText.'); 
	};
	
	
});