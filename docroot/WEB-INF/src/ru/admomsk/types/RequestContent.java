package ru.admomsk.types;

import java.io.File;
import java.util.ArrayList;

public class RequestContent {
	//Цель получения архивного документа
	private String purposeDocumentArchive = "";
	//Что необходимо подвердить
	private ArrayList<String> whatNeededConfirm;
	//Где работал заявитель
	private String whereApplicantWorked = "";
	//Вид архивного документа
	private String viewArchivedDocuments = "";
	
	private String type;
	
	private String personInfoLastName = "";
	private String personInfoFirstName = "";
	private String personInfoMiddleName = "";
	private String personInfoChangeLastName = "";
	private ArrayList<String> personInfoDatesBirth;
	
	private String infoRequestOrganization = "";
	private String dateStartWork = "";
	private String dateEndWork = "";
	private String position = "";
	private File copyWorkRecord = null;
	private String otherInformationWork = "";
	
	private String nameSchool = "";
	private String numberSchool = "";
	private String dateStartStudy = "";
	private String dateEndStudy = "";
	private String otherInformationStudy = "";
	
	
	public void setPurposeDocumentArchive(String purposeDocumentArchive) {
		this.purposeDocumentArchive = purposeDocumentArchive;
	}
	public String getPurposeDocumentArchive() {
		return purposeDocumentArchive;
	}
	
	public void setWhereApplicantWorked(String whereApplicantWorked) {
		this.whereApplicantWorked = whereApplicantWorked;
	}
	public String getWhereApplicantWorked() {
		return whereApplicantWorked;
	}
	
	public void setWhatNeededConfirm(ArrayList<String> whatNeededConfirm) {
		this.whatNeededConfirm = whatNeededConfirm;
	}
	public ArrayList<String> getWhatNeededConfirm() {
		return whatNeededConfirm;
	}
	
	public void setViewArchivedDocuments(String viewArchivedDocuments) {
		this.viewArchivedDocuments = viewArchivedDocuments;
	}
	public String getViewArchivedDocuments() {
		return viewArchivedDocuments;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	
	public void setPersonInfoLastName(String personInfoLastName) {
		this.personInfoLastName = personInfoLastName;
	}
	public String getPersonInfoLastName() {
		return personInfoLastName;
	}
	
	public void setPersonInfoFirstName(String personInfoFirstName) {
		this.personInfoFirstName = personInfoFirstName;
	}
	public String getPersonInfoFirstName() {
		return personInfoFirstName;
	}
	
	public void setPersonInfoMiddleName(String personInfoMiddleName) {
		this.personInfoMiddleName = personInfoMiddleName;
	}
	public String getPersonInfoMiddleName() {
		return personInfoMiddleName;
	}
	
	public void setPersonInfoChangeLastName(
			String personInfoChangeLastName) {
		this.personInfoChangeLastName = personInfoChangeLastName;
	}
	public String getPersonInfoChangeLastName() {
		return personInfoChangeLastName;
	}
	
	public void setPersonInfoDatesBirth(
			ArrayList<String> personInfoDatesBirth) {
		this.personInfoDatesBirth = personInfoDatesBirth;
	}
	public ArrayList<String> getPersonInfoDatesBirth() {
		return personInfoDatesBirth;
	}
	
	public void setInfoRequestOrganization(String infoRequestOrganization) {
		this.infoRequestOrganization = infoRequestOrganization;
	}
	public String getInfoRequestOrganization() {
		return infoRequestOrganization;
	}
	
	public void setDateStartWork(String dateStartWork) {
		this.dateStartWork = dateStartWork;
	}
	public String getDateStartWork() {
		return dateStartWork;
	}
	
	public void setDateEndWork(String dateEndWork) {
		this.dateEndWork = dateEndWork;
	}
	public String getDateEndWork() {
		return dateEndWork;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition() {
		return position;
	}
	
	public void setCopyWorkRecord(File copyWorkRecord) {
		this.copyWorkRecord = copyWorkRecord;
	}
	public File getCopyWorkRecord() {
		return copyWorkRecord;
	}
	
	public void setOtherInformationWork(String otherInformationWork) {
		this.otherInformationWork = otherInformationWork;
	}
	public String getOtherInformationWork() {
		return otherInformationWork;
	}
	
	public void setNameSchool(String nameSchool) {
		this.nameSchool = nameSchool;
	}
	public String getNameSchool() {
		return nameSchool;
	}
	
	public void setNumberSchool(String numberSchool) {
		this.numberSchool = numberSchool;
	}
	public String getNumberSchool() {
		return numberSchool;
	}
	
	public void setDateStartStudy(String dateStartStudy) {
		this.dateStartStudy = dateStartStudy;
	}
	public String getDateStartStudy() {
		return dateStartStudy;
	}
	
	public void setDateEndStudy(String dateEndStudy) {
		this.dateEndStudy = dateEndStudy;
	}
	public String getDateEndStudy() {
		return dateEndStudy;
	}
	
	public void setOtherInformationStudy(String otherInformationStudy) {
		this.otherInformationStudy = otherInformationStudy;
	}
	public String getOtherInformationStudy() {
		return otherInformationStudy;
	}
	
}
