package ru.admomsk.server;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.upload.UploadServletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import ru.admomsk.types.Individual;
import ru.admomsk.types.Legal;
import ru.admomsk.types.RequestContent;
import ru.admomsk.utils.FileUtils;
import ru.admomsk.utils.SendMailUsage;

/**
 * Portlet implementation class ArchiveReference
 */
public class ArchiveReference extends GenericPortlet {

	final private Double maxSizeFile = 5.0*(1024.0*1024.0);
	final private String allowExt = "zip|pdf|doc|docx|jpg|jpeg|png|gif|bmp|tif|tiff";
	
    public void init() {
        viewJSP = getInitParameter("view-jsp");
    }
    
    public void doView(
            RenderRequest renderRequest, RenderResponse renderResponse)
        throws IOException, PortletException {
        
        include(viewJSP, renderRequest, renderResponse);
    }

    protected void include(
            String path, RenderRequest renderRequest,
            RenderResponse renderResponse)
        throws IOException, PortletException {

        PortletRequestDispatcher portletRequestDispatcher =
            getPortletContext().getRequestDispatcher(path);

        if (portletRequestDispatcher == null) {
            _log.error(path + " is not a valid include");
        }
        else {
            portletRequestDispatcher.include(renderRequest, renderResponse);
        }
    }
    
    @Override
    public void processAction(ActionRequest request, ActionResponse response)
    		throws PortletException, IOException {
    	// TODO Auto-generated method stub
    	super.processAction(request, response);
    }
    
    
    @Override
    public void serveResource(ResourceRequest request, ResourceResponse response)
    		throws PortletException, IOException {
    	
    	//response.setCharacterEncoding("utf-8");
    	//System.out.println("Ну да что то пришло");
    	//response.getWriter().println("да да что то пришло");
    	//класс содержимого заппроса
    	RequestContent requestContent = new RequestContent();
    	//класс содержимого физического лица
    	Individual individual = new Individual();
    	//класс юридического лица
    	Legal legal = new Legal();
    	//System.out.println("Ох ты 1 " + request.getAttribute("name").toString());
    	/*System.out.println("Ох ты 2 " + request.getParameter("individual_last_name"));
    	HttpServletRequest portalHttpReq = PortalUtil.getHttpServletRequest(request);
    	System.out.println("Ох ты 3 " + portalHttpReq.getParameter("individual_last_name"));
    	System.out.println("Ох ты 4 " + ParamUtil.getString(request, "individual_last_name"));
    	System.out.println("Ох ты 5 " + ParamUtil.getString(portalHttpReq, "individual_last_name"));
    	
		UploadServletRequest usr = PortalUtil.getUploadServletRequest(portalHttpReq);
		System.out.println("Ох ты 7 " + usr.getParameter("individual_last_name"));
		
    	if (true) return;
    	*/
    	//Получить параметры запроса
		//UploadPortletRequest usr = PortalUtil.getUploadPortletRequest(request);
    	HttpServletRequest hsr = PortalUtil.getHttpServletRequest(request);
    	UploadServletRequest usr = PortalUtil.getUploadServletRequest(hsr);
		System.out.println("Ох ты 6 " + usr.getParameter("individual_last_name"));
    	
		//временная
		String temp;
		//счетчик
		int n = 1;
		
		//Копия трудовой книжки (при наличии)
		File uploadFile;
		
		System.out.println("name="+usr.getFullFileName("copy_work_book"));
		System.out.println("name="+usr.getFile("copy_work_book").getAbsolutePath());
		System.out.println("name="+usr.getFile("copy_work_book").getCanonicalPath());
		System.out.println("name="+usr.getFile("copy_work_book").getPath());
		System.out.println("name="+usr.getFile("copy_work_book").getParent());
		System.out.println("length="+usr.getFile("copy_work_book").length());
		
		if (!usr.getFullFileName("copy_work_book").equals("")) {
			uploadFile = usr.getFile("copy_work_book");
			//проверка на максимальный размер файла
			if (uploadFile.length() <= maxSizeFile) {
				//проверка на допустимые расширения файла
				if (allowExt.indexOf(FileUtils.getExtNotDot(uploadFile.getName())) == -1) {
					//response.getWriter().println("Данный формат файла не поддерживается!");
					System.out.println("not_file_format");
					response.getWriter().println("not_file_format");
					uploadFile.delete();
				} else {
					requestContent.setCopyWorkRecord(uploadFile);
					//response.getWriter().println("Файл успешно загружен!");
					System.out.println("file_success_uploaded");
					response.getWriter().println("file_success_uploaded");
				}
			} else {
				//response.getWriter().println("Файл превышает размер 5 мб!");
				System.out.println("file_more_max_size");
				response.getWriter().println("file_more_max_size");
				uploadFile.delete();
			}
		}
		
		//sendMessage(requestContent, individual, legal);
		
		//if (true) return;
		
		//Получить цель архивного документа
		temp = usr.getParameter("purpose");
		//если выбрано иное(other), то получить из текстового поля (purpose_other)
		if (temp.equals("other")){
			requestContent.setPurposeDocumentArchive(usr.getParameter("purpose_other"));
		} else {
			requestContent.setPurposeDocumentArchive(usr.getParameter("purpose"));
		}
		
		//Что необходимо подвердить
		ArrayList<String> whatNeededConfirm = new ArrayList<String>();
		temp = usr.getParameter("what_needed_confirm");
		if (temp.equals("факты о работе заявителя")){
			if ((temp = usr.getParameter("what_needed_confirm1")) != null) whatNeededConfirm.add(temp);
			if ((temp = usr.getParameter("what_needed_confirm2")) != null) whatNeededConfirm.add(temp);
			if ((temp = usr.getParameter("what_needed_confirm3")) != null) whatNeededConfirm.add(temp);
			if ((temp = usr.getParameter("what_needed_confirm4")) != null) whatNeededConfirm.add(temp);
		} else {
			whatNeededConfirm.add(temp);
		}
		requestContent.setWhatNeededConfirm(whatNeededConfirm);
		
		//Где работал заявитель
		requestContent.setWhereApplicantWorked(usr.getParameter("where_applicant_worked"));
		
		//Вид архивного документа
		requestContent.setViewArchivedDocuments(usr.getParameter("view_document_archive"));
		
		//Тип заявителя
		requestContent.setType(usr.getParameter("info_applicant"));
		
		if (requestContent.getType().equals("individual")) {
			//ФИО
			individual.setLastName(usr.getParameter("individual_last_name"));
			individual.setFirstName(usr.getParameter("individual_first_name"));
			individual.setMiddleName(usr.getParameter("individual_middle_name"));
			
			//Смена имени
			if(usr.getParameter("individual_change_name") != null) {
				individual.setChangeLastName(usr.getParameter("text_icn"));
			}
			
			//Паспорт
			individual.setSeriesPassport(usr.getParameter("individual_series_passport"));
			individual.setNumberPassport(usr.getParameter("individual_number_passport"));
			
			//Кем выдан паспорт
			individual.setWhoGivePassport(usr.getParameter("individual_who_give_passport"));
			
			//Дата выдачи паспотра
			individual.setDateIssuePassport(usr.getParameter("individual_date_issue_passport"));
			
			//Адрес
			
			//Страна
			individual.setCountry(usr.getParameter("individual_сountry"));
			
			//Регион
			individual.setRegion(usr.getParameter("individual_region"));
			
			//Город
			individual.setCity(usr.getParameter("individual_city"));
			
			//Улица
			individual.setStreet(usr.getParameter("individual_street"));
			
			//Дом
			individual.setHome(usr.getParameter("individual_home"));
			
			//Корпус
			individual.setHousing(usr.getParameter("individual_housing"));
			
			//Квартира
			individual.setApartment(usr.getParameter("individual_apartment"));
			
			//Даты рождения детей
			n = 1;
			ArrayList<String> datesBirth = new ArrayList<String>();
			while((temp = usr.getParameter("individual_dates_birth"+n)) != null){
				datesBirth.add(temp);
				n++;
			}
			individual.setDatesBirth(datesBirth);
			
			//Номер телефона
			individual.setNumberTelephone(usr.getParameter("individual_number_telephone"));
			
			//Адрес электронной почты
			individual.setEmail(usr.getParameter("individual_email"));
			
		} else {
			
			//Полное наименование
			legal.setFullName(usr.getParameter("legal_full_name"));
			
			//Реквизиты
			
			//ОГРН
			legal.setBIN(usr.getParameter("legal_BIN"));
			
			//ИНН
			legal.setINN(usr.getParameter("legal_INN"));
			
			//Руководитель
			//Фамилия
			legal.setLastName(usr.getParameter("legal_last_name"));
			
			//Имя
			legal.setFirstName(usr.getParameter("legal_first_name"));
			
			//Отчество
			legal.setMiddleName(usr.getParameter("legal_middle_name"));
			
			//Должность руководителя
			legal.setPostHead(usr.getParameter("legal_post_head"));
			
			//Почтовый адрес
			//Страна
			legal.setCountry(usr.getParameter("legal_сountry"));
			
			//Регион
			legal.setRegion(usr.getParameter("legal_region"));
			
			//Город
			legal.setCity(usr.getParameter("legal_city"));
			
			//Улица
			legal.setStreet(usr.getParameter("legal_street"));
			
			//Дом
			legal.setHome(usr.getParameter("legal_home"));
			
			//Корпус
			legal.setHousing(usr.getParameter("legal_housing"));
			
			//Квартира
			legal.setApartment(usr.getParameter("legal_apartment"));
			
			//Телефон
			legal.setNumberTelephone(usr.getParameter("legal_number_telephone"));
			
			//Адрес электронной почты
			legal.setEmail(usr.getParameter("legal_email"));
			
		}
		
		//Введите данные лица, в отношении которого запрашивается информация
		
		//Фамилия
		requestContent.setPersonInfoLastName(usr.getParameter("pi_last_name"));
		
		//Имя
		requestContent.setPersonInfoFirstName(usr.getParameter("pi_first_name"));
		
		//Отчество
		requestContent.setPersonInfoFirstName(usr.getParameter("pi_middle_name"));
		
		//Заявитель менял фамилию, имя или отчество
		requestContent.setPersonInfoChangeLastName(usr.getParameter("text_picn"));
		
		//Даты рождения детей
		n = 1;
		ArrayList<String> datesBirth = new ArrayList<String>();
		while((temp = usr.getParameter("layout_pi_dates_birth"+n)) != null){
			datesBirth.add(temp);
			n++;
		}
		requestContent.setPersonInfoDatesBirth(datesBirth);
		
		//Сведения о запрашиваемой организации: полное и сокращенное название, переименования, ведомственная принадлежность<div class="required">*</div></label>
		requestContent.setInfoRequestOrganization(usr.getParameter("info_req_org"));
		
		//Дата начала трудовой деятельности в организации
		requestContent.setInfoRequestOrganization(usr.getParameter("start_work_org"));
		
		//Дата окончания трудовой деятельности в организации
		requestContent.setInfoRequestOrganization(usr.getParameter("end_work_org"));
		
		//Занимаемая должность
		requestContent.setInfoRequestOrganization(usr.getParameter("position"));
		
		
		//Иные сведения, облегчающие поиск необходимой информации
		requestContent.setOtherInformationWork(usr.getParameter("search_info_req_org"));		
		
		//Введите сведения по запрашиваемой школе
		//Наименование школы
		requestContent.setNameSchool(usr.getParameter("name_school"));
		
		//Номер школы
		requestContent.setNumberSchool(usr.getParameter("num_school"));
		
		//Дата начала обучения в школе
		requestContent.setDateStartStudy(usr.getParameter("start_study_org"));
		
		//Дата окончания обучения в школе
		requestContent.setDateEndStudy(usr.getParameter("end_study_org"));
		
		//Иные сведения, облегчающие поиск необходимой информации
		requestContent.setOtherInformationWork(usr.getParameter("search_info_req_school"));
		
		/*
		System.out.println("usr.getFileName(\"cwb\") = " + usr.getFileName("cwb"));
		System.out.println("usr.getFile(\"cwb\").length() = " + usr.getFile("cwb").length());
		System.out.println("usr.getParameter(\"individual_last_name\") = " + usr.getParameter("individual_last_name"));
		System.out.println("usr.getParameter(\"cwb\") = " + usr.getParameter("cwb"));
		System.out.println("usr.getParameter(\"purpose\") = " + usr.getParameter("purpose"));
		System.out.println("usr.getParameter(\"what_needed_confirm1\") = " + usr.getParameter("what_needed_confirm1"));
		System.out.println("usr.getParameter(\"what_needed_confirm2\") = " + usr.getParameter("what_needed_confirm2"));
		System.out.println("usr.getParameter(\"what_needed_confirm3\") = " + usr.getParameter("what_needed_confirm3"));
		System.out.println("usr.getParameter(\"what_needed_confirm4\") = " + usr.getParameter("what_needed_confirm4"));
		System.out.println("usr.getParameter(\"individual_change_name\") = " + usr.getParameter("individual_change_name"));
		*/
		//System.out.println(usr.getParameter("cwb"));
		
		sendMessage(requestContent, individual, legal);
		
		
		response.getWriter().println("success_get_all_parametrs");
    	//super.serveResource(request, response);
    }
    
    protected String viewJSP;

    private static Log _log = LogFactoryUtil.getLog(ArchiveReference.class);
    
    @SuppressWarnings("finally")
	public static boolean sendMessage (RequestContent requestContent, Individual individual, Legal legal) {
		
		//request.xml (создать временно)
		File request = null;
		File requestDir = null;
		boolean result = false;
		ArrayList<File> attach = null;
		try {
			SendMailUsage sendMailUsage = new SendMailUsage();
			//sendMailUsage.setFrom("ilya.false@gmail.com");
			sendMailUsage.setFrom("ialozhnikov@admomsk.ru");
			//sendMailUsage.setFrom("noreply@admomsk.ru");
			//sendMailUsage.setTo("ilya.lozhnikov13@gmail.com");
			sendMailUsage.setTo("ialozhnikov@admomsk.ru");
			//sendMailUsage.setTo("coosedd@admomsk.ru");
			//стандартный
			//sendMailUsage.setHost("smtp.gmail.com");
			//localhost gmail
			//sendMailUsage.setHost("10.0.0.6");
			//с пробного сервака gmail?
			//sendMailUsage.setHost("217.25.215.18");
			//localhost admomsk
			sendMailUsage.setHost("10.0.0.29");
			//с портала admomsk
			//sendMailUsage.setHost("217.25.215.29");
			//ошибка
			//sendMailUsage.setHost("1.1.1.1");
			
			//sendMailUsage.setUsername("ilya.false");
			//sendMailUsage.setUsername("noreply");
			sendMailUsage.setUsername("Portal");
			//sendMailUsage.setPassword("ialoz");
			sendMailUsage.setSubject("1234");
			//sendMailUsage.setDate(Calendar.getInstance().getTime());
			sendMailUsage.setText("");
			
			//Создать request.xml
			PrintWriter pw = null;
			//создать временную папку для request.xml
			requestDir = File.createTempFile("request", "");
			requestDir.delete();
			requestDir.mkdir();
			if (!requestDir.isDirectory()) return false;
			
			//создать request.xml
			request = new File(requestDir, "request.xml");
			
			try {
				pw = new PrintWriter(new FileOutputStream(request));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
			/*Записать данные request xml*/
			
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			pw.println("<ArchivedReference xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UserRequests.xsd\">");
			pw.println("<applicantType>"+requestContent.getType()+"</applicantType>");
			
			pw.println("<login>"+individual.getEmail()+"</login>");
			pw.println("<nameLast>"+individual.getLastName()+"</nameLast>");
			pw.println("<nameFirst>"+individual.getFirstName()+"</nameFirst>");
			pw.println("<nameMiddle>"+individual.getMiddleName()+"</nameMiddle>");
			pw.println("<alterName>"+individual.getChangeLastName()+"</alterName>");
			//проверка на пустоту
			if (requestContent.getType().equals("individual")) {
			pw.println("<passport>"+
					individual.getSeriesPassport()+"; "+
					individual.getNumberPassport()+"; "+
					individual.getWhoGivePassport()+"; "+
					individual.getDateIssuePassport()
					+"</passport>");
			} else pw.println("<passport></passport>");
			pw.println("<phone>"+individual.getNumberTelephone()+"</phone>");
			pw.println("<country>"+individual.getCountry()+"</country>");
			pw.println("<state></state>");
			pw.println("<region>"+individual.getRegion()+"</region>"); 
			pw.println("<city>"+individual.getCity()+"</city>");
			pw.println("<locality></locality>");
			pw.println("<street>"+individual.getStreet()+"</street>");
			pw.println("<house>"+individual.getHome()+"</house>");
			pw.println("<corp>"+individual.getHousing()+"</corp>");
			pw.println("<flat>"+individual.getApartment()+"</flat>");
			
			ArrayList<String> alDatesBirth = individual.getDatesBirth();
			if (alDatesBirth != null) {
				for (String datesBirth:alDatesBirth) {
					pw.println("<births>"+datesBirth+"</births>");
				}
				//если нет дат, то записать пустой нод
				if (alDatesBirth.size() == 0) pw.println("<births></births>"); 
			} else {
				pw.println("<births></births>");
			}
			
			pw.println("<zipCode></zipCode>");
			pw.println("<section></section>");
			pw.println("<room></room>");
			pw.println("<orgName>"+legal.getFullName()+"</orgName>");
			pw.println("<orgOgrn>"+legal.getBIN()+"</orgOgrn>");
			pw.println("<orgInn>"+legal.getINN()+"</orgInn>");
			pw.println("<orgDirectorLastName>"+legal.getLastName()+"</orgDirectorLastName>");
			pw.println("<orgDirectorLastFirst>"+legal.getFirstName()+"</orgDirectorLastFirst>");
			pw.println("<orgDirectorLastMiddle>"+legal.getMiddleName()+"</orgDirectorLastMiddle>");
			pw.println("<orgDirectorPost>"+legal.getPostHead()+"</orgDirectorPost>");
			
			//проверка на пустоту
			String legalAddress = "";
			//если выбрано юр. лицо
			if (requestContent.getType().equals("legal")) {
				legalAddress = legal.getCountry() + ", ";
				if (!legal.getRegion().equals("")) {
					legalAddress = legalAddress + legal.getRegion() + ", ";
				}
				legalAddress = legalAddress + legal.getCity() + ", ";
				legalAddress = legalAddress + legal.getStreet() + " ";
				legalAddress = legalAddress + legal.getHome();
				if (!legal.getHousing().equals("")) {
					legalAddress = legalAddress + ", корп. "+ legal.getHousing();
				}			
				if (!legal.getApartment().equals("")) {
					legalAddress = legalAddress + ", кв. "+ legal.getApartment();
				}
			}
			pw.println("<orgAddress>"+legalAddress+"</orgAddress>");
			
			/*if (!legal.getCountry().equals("")) {
				pw.println("<orgAddress>"
						+legal.getCountry()+" "
						+legal.getRegion()+" "
						+legal.getCity()+" "
						+legal.getStreet()+" "
						+legal.getHome()+" "
						+legal.getHousing()+" "
						+legal.getApartment()
					+"</orgAddress>");
			} else pw.println("<orgAddress></orgAddress>");*/
			
			/*pw.println("<orgAddress>"+legal.getMailAddress()+"</orgAddress>");*/
			pw.println("<orgTelefon>"+legal.getNumberTelephone()+"</orgTelefon>");
			pw.println("<orgEmail>"+legal.getEmail()+"</orgEmail>");
			
			pw.println("<nameLast>"+requestContent.getPersonInfoLastName()+"</nameLast>");
			pw.println("<nameFirst>"+requestContent.getPersonInfoFirstName()+"</nameFirst>");
			pw.println("<nameMiddle>"+requestContent.getPersonInfoMiddleName()+"</nameMiddle>");
			pw.println("<alterName>"+requestContent.getPersonInfoChangeLastName()+"</alterName>");
			
			alDatesBirth = requestContent.getPersonInfoDatesBirth();
			if (alDatesBirth != null) {
				for (String datesBirth:alDatesBirth) {
					pw.println("<births>"+datesBirth+"</births>");
				}
				//если нет дат, то записать пустой нод
				if (alDatesBirth.size() == 0) pw.println("<births></births>"); 
			} else {
				pw.println("<births></births>");
			}
			
			pw.println("<purposeRequest>"+requestContent.getPurposeDocumentArchive()+"</purposeRequest>");
			
			String whereApplicantWorked = "";
			if (requestContent.getWhereApplicantWorked() != null) {
				if (requestContent.getWhereApplicantWorked().equals("мунципальная система образования")) whereApplicantWorked = "a";
				if (requestContent.getWhereApplicantWorked().equals("иные ликвидированные, реорганизованные организации города Омска")) whereApplicantWorked = "b";
			}
			pw.println("<workApplicant>"+whereApplicantWorked+"</workApplicant>");
			
			
			//собрать строку из значений Что необходимо подтвердить?*
			boolean factStudy = false;
			String str = "";
			int size = requestContent.getWhatNeededConfirm().size();
			for (int i=0; i<size; i++){
				str = requestContent.getWhatNeededConfirm().get(i);
				if (str.equals("стаж работы;")) {
					pw.println("<neededConfirm>"+"1"+"</neededConfirm>");
				}
				if (str.equals("льготный трудовой стаж работы;")) {
					pw.println("<neededConfirm>"+"2"+"</neededConfirm>");
				}
				if (str.equals("размер заработной платы;")) {
					pw.println("<neededConfirm>"+"3"+"</neededConfirm>");
				}
				if (str.equals("факт переименования, реорганизации, ликвидации организации;")) {
					pw.println("<neededConfirm>"+"4"+"</neededConfirm>");
				}
				if (str.equals("факт обучения в школе.")) {
					pw.println("<neededConfirm>"+"5"+"</neededConfirm>");
					factStudy = true;
				}
				//присоединить пробел если не последний
				//if (i<size-1) str = str.concat(" ");
				//склеить значения
				//whatNeededConfirm = whatNeededConfirm.concat(str);
			}
			//pw.println("<neededConfirm>"+whatNeededConfirm+"</neededConfirm>");
			pw.println("<docArchival>"+requestContent.getViewArchivedDocuments()+"</docArchival>");
			//Если выбран "факт обучения в школе."
			if (factStudy) {
				pw.println("<requestingOrganization>"+requestContent.getNameSchool()+" "+requestContent.getNumberSchool()+"</requestingOrganization>");
				pw.println("<startingWork>"+requestContent.getDateStartStudy()+"</startingWork>");
				pw.println("<completionWork>"+requestContent.getDateEndStudy()+"</completionWork>");
				pw.println("<workPost></workPost>");
				pw.println("<docOther>"+requestContent.getOtherInformationStudy()+"</docOther>");
			} else {
				pw.println("<requestingOrganization>"+requestContent.getInfoRequestOrganization()+"</requestingOrganization>");
				pw.println("<startingWork>"+requestContent.getDateStartWork()+"</startingWork>");
				pw.println("<completionWork>"+requestContent.getDateEndWork()+"</completionWork>");
				pw.println("<workPost>"+requestContent.getPosition()+"</workPost>");
				pw.println("<docOther>"+requestContent.getOtherInformationWork()+"</docOther>");
			}
			pw.println("<depCode>61</depCode>");
			pw.println("<depCodeSub>72</depCodeSub>");
	        
			File rename = null;
			if (requestContent.getCopyWorkRecord() != null) {
				//Расширение с точкой
				String ext = FileUtils.getExt(requestContent.getCopyWorkRecord().getName());
				rename = new File("file1" + ext);
				//rename = new File(requestDir.getPath()+File.separator+"file1" + ext);
				//rename = new File(requestDir, "file1" + ext);
				requestContent.getCopyWorkRecord().renameTo(rename);
				//requestContent.getCopyWorkRecord().delete();
				//requestContent.setCopyWorkRecord(rename);
				pw.println("<file>"+rename.getName()+"</file>");
				
			} else {
				pw.println("<file></file>");
			}
			pw.println("</ArchivedReference>");
			
			
			pw.close();
			
			//Прикрепить request.xml
			attach = new ArrayList<File>();
			attach.add(request);
			//если есть файл то присобачить его к письму
			if (rename != null) attach.add(rename);
			sendMailUsage.setFileAsAttachment(attach);
			
			//Послать email
			result = sendMailUsage.sendMessage();
		
		} catch (Exception e) {
			System.out.println("---error sending mail---");
			result = false;
			e.printStackTrace();
			//удалить файлы
			for (File a:attach) {
				System.out.println("remove "+a.getName());
				a.delete();
			}
			System.out.println("remove "+requestDir.getName());
			requestDir.delete();
			if (requestContent.getCopyWorkRecord() != null) System.out.println("remove "+requestContent.getCopyWorkRecord().getName());
		} finally {
			//удалить файлы
			for (File a:attach) {
				System.out.println("remove "+a.getName());
				a.delete();
			}
			System.out.println("remove "+requestDir.getName());
			requestDir.delete();
			if (requestContent.getCopyWorkRecord() != null) System.out.println("remove "+requestContent.getCopyWorkRecord().getName());
			return result;
		}
	}

}
